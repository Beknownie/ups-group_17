EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 60B7E619
P 5050 3800
F 0 "D1" V 4997 3878 50  0000 L CNN
F 1 "NSCW100" V 5088 3878 50  0001 L CNN
F 2 "LED_THT:LED_D3.0mm" H 5050 3800 50  0001 C CNN
F 3 "~" H 5050 3800 50  0001 C CNN
	1    5050 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 60B7F020
P 5050 4200
F 0 "R2" H 5120 4246 50  0000 L CNN
F 1 "330" H 5120 4155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4980 4200 50  0001 C CNN
F 3 "~" H 5050 4200 50  0001 C CNN
	1    5050 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4400 5050 4400
Wire Wire Line
	5050 4400 5050 4350
Wire Wire Line
	5050 4050 5050 3950
$Comp
L Device:C C1
U 1 1 60B80D98
P 4700 4150
F 0 "C1" H 4815 4196 50  0000 L CNN
F 1 "1µ" H 4815 4105 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 4738 4000 50  0001 C CNN
F 3 "~" H 4700 4150 50  0001 C CNN
	1    4700 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3650 4700 4000
Wire Wire Line
	4700 4300 4700 4500
$Comp
L Device:R R1
U 1 1 60B82737
P 4900 4500
F 0 "R1" H 4970 4546 50  0000 L CNN
F 1 "1.65k" H 4970 4455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4830 4500 50  0001 C CNN
F 3 "~" H 4900 4500 50  0001 C CNN
	1    4900 4500
	0    1    1    0   
$EndComp
Text GLabel 5700 3400 1    50   Input ~ 0
Vin
Wire Wire Line
	5700 3400 5700 3650
Connection ~ 5700 3650
Wire Wire Line
	5700 3650 5700 4100
$Comp
L Project-schematic-rework-rescue:USB_A-Connector J1
U 1 1 60B9632B
P 1200 3150
F 0 "J1" H 1257 3617 50  0000 C CNN
F 1 "USB_A" H 1257 3526 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Lumberg_2486_01_Horizontal" H 1350 3100 50  0001 C CNN
F 3 " ~" H 1350 3100 50  0001 C CNN
	1    1200 3150
	1    0    0    -1  
$EndComp
Text GLabel 1500 2950 2    50   Input ~ 0
Vcc
Text GLabel 1150 3600 3    50   Input ~ 0
Vgnd
NoConn ~ 1500 3150
NoConn ~ 1500 3250
Wire Wire Line
	1100 3550 1150 3550
Wire Wire Line
	1150 3600 1150 3550
Connection ~ 1150 3550
Wire Wire Line
	1150 3550 1200 3550
$Comp
L Battery_Management:LTC4054ES5-4.2 U1
U 1 1 60B6B3B2
P 5700 4400
F 0 "U1" H 6144 4396 50  0000 C TNN
F 1 "LTC4054ES5-4.2" H 6144 4305 50  0000 C TNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 5700 3900 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/405442xf.pdf" H 5700 4300 50  0001 C CNN
	1    5700 4400
	1    0    0    -1  
$EndComp
Text GLabel 5200 4800 3    50   Input ~ 0
Vgnd
Wire Wire Line
	5050 4500 5300 4500
Wire Wire Line
	4750 4500 4700 4500
Wire Wire Line
	5700 4800 4700 4800
Wire Wire Line
	4700 4800 4700 4500
Connection ~ 4700 4500
Text GLabel 1000 2250 3    50   Input ~ 0
Vbat
Text GLabel 1400 2250 3    50   Input ~ 0
Vgnd
Wire Wire Line
	4700 3650 5050 3650
Connection ~ 5050 3650
Wire Wire Line
	5050 3650 5700 3650
Wire Wire Line
	6100 4400 6350 4400
Text GLabel 6350 4400 2    50   Input ~ 0
Vbat
Text Notes 7350 7550 0    118  ~ 0
Power supply schematic\n
$Comp
L Mechanical:MountingHole_Pad Bat+1
U 1 1 60C4AD21
P 1000 2150
F 0 "Bat+1" H 1100 2153 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 2108 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1000 2150 50  0001 C CNN
F 3 "~" H 1000 2150 50  0001 C CNN
	1    1000 2150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad Bat-1
U 1 1 60C4B5D3
P 1400 2150
F 0 "Bat-1" H 1500 2153 50  0000 L CNN
F 1 "MountingHole_Pad" H 1500 2108 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1400 2150 50  0001 C CNN
F 3 "~" H 1400 2150 50  0001 C CNN
	1    1400 2150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
