EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Project-schematic-rework-rescue:USB_A-Connector J1
U 1 1 60B9632B
P 1200 3150
F 0 "J1" H 1257 3617 50  0000 C CNN
F 1 "USB_A" H 1257 3526 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Lumberg_2486_01_Horizontal" H 1350 3100 50  0001 C CNN
F 3 " ~" H 1350 3100 50  0001 C CNN
	1    1200 3150
	1    0    0    -1  
$EndComp
Text GLabel 1500 2950 2    50   Input ~ 0
Vcc
Text GLabel 1150 3600 3    50   Input ~ 0
Vgnd
NoConn ~ 1500 3150
NoConn ~ 1500 3250
Wire Wire Line
	1100 3550 1150 3550
Wire Wire Line
	1150 3600 1150 3550
Connection ~ 1150 3550
Wire Wire Line
	1150 3550 1200 3550
Text GLabel 1000 2250 3    50   Input ~ 0
Vbat
Text GLabel 1400 2250 3    50   Input ~ 0
Vgnd
$Comp
L Mechanical:MountingHole_Pad Bat+1
U 1 1 60C4AD21
P 1000 2150
F 0 "Bat+1" H 1100 2153 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 2108 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1000 2150 50  0001 C CNN
F 3 "~" H 1000 2150 50  0001 C CNN
	1    1000 2150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad Bat-1
U 1 1 60C4B5D3
P 1400 2150
F 0 "Bat-1" H 1500 2153 50  0000 L CNN
F 1 "MountingHole_Pad" H 1500 2108 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1400 2150 50  0001 C CNN
F 3 "~" H 1400 2150 50  0001 C CNN
	1    1400 2150
	1    0    0    -1  
$EndComp
Text Notes 7350 7550 0    118  ~ 0
Amplifier schematic
Text GLabel 5100 4600 3    50   Input ~ 0
Vgnd
Wire Wire Line
	5700 3250 6050 3250
Wire Wire Line
	5700 3350 5700 3250
Connection ~ 5700 3250
Wire Wire Line
	5500 3250 5700 3250
Wire Wire Line
	5500 3700 5500 3250
Wire Wire Line
	5500 3800 5700 3800
Wire Wire Line
	5700 3650 5700 3800
Wire Wire Line
	6050 3250 6050 3450
Connection ~ 5500 3250
Wire Wire Line
	4600 3250 5500 3250
Wire Wire Line
	6050 3900 5500 3900
Wire Wire Line
	6050 3900 6150 3900
Connection ~ 6050 3900
Wire Wire Line
	6050 3750 6050 3900
Wire Wire Line
	6850 3900 6500 3900
Wire Wire Line
	6850 4000 6850 3900
Connection ~ 6850 3900
Wire Wire Line
	7250 3900 6850 3900
Wire Wire Line
	5500 4200 5500 4300
Wire Wire Line
	6850 4300 5500 4300
$Comp
L Device:R R17
U 1 1 60C83DFB
P 6850 4450
F 0 "R17" H 6920 4496 50  0000 L CNN
F 1 "2.2k" H 6920 4405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6780 4450 50  0001 C CNN
F 3 "~" H 6850 4450 50  0001 C CNN
	1    6850 4450
	1    0    0    -1  
$EndComp
Connection ~ 6850 4300
$Comp
L Device:R R16
U 1 1 60C830D7
P 6850 4150
F 0 "R16" H 6920 4196 50  0000 L CNN
F 1 "6.8k" H 6920 4105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6780 4150 50  0001 C CNN
F 3 "~" H 6850 4150 50  0001 C CNN
	1    6850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4600 4050 4600
Connection ~ 4700 4600
Wire Wire Line
	4700 4500 4700 4600
Wire Wire Line
	4700 4100 4700 4200
$Comp
L Device:C C2
U 1 1 60C79B31
P 4700 4350
F 0 "C2" H 4815 4396 50  0000 L CNN
F 1 "1.5n" H 4815 4305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 4738 4200 50  0001 C CNN
F 3 "~" H 4700 4350 50  0001 C CNN
	1    4700 4350
	-1   0    0    1   
$EndComp
Connection ~ 5750 4600
Wire Wire Line
	5750 4100 5750 4600
Wire Wire Line
	5500 4100 5750 4100
Wire Wire Line
	5750 4600 6500 4600
Wire Wire Line
	6500 4600 6850 4600
Connection ~ 6500 4600
Wire Wire Line
	6500 4350 6500 4600
Wire Wire Line
	6500 4050 6500 3900
Connection ~ 6500 3900
Wire Wire Line
	6450 3900 6500 3900
$Comp
L Device:C C3
U 1 1 60C69061
P 6500 4200
F 0 "C3" H 6615 4246 50  0000 L CNN
F 1 "0.22m" H 6615 4155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 6538 4050 50  0001 C CNN
F 3 "~" H 6500 4200 50  0001 C CNN
	1    6500 4200
	-1   0    0    1   
$EndComp
$Comp
L Device:L L1
U 1 1 60C513FF
P 6050 3600
F 0 "L1" V 6240 3600 50  0000 C CNN
F 1 "18.91u" V 6149 3600 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L6.6mm_D2.7mm_P10.16mm_Horizontal_Vishay_IM-2" H 6050 3600 50  0001 C CNN
F 3 "~" H 6050 3600 50  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D6
U 1 1 60C49AB0
P 6300 3900
F 0 "D6" H 6300 3776 50  0000 C CNN
F 1 "D" V 6345 3979 50  0001 L CNN
F 2 "Diode_THT:D_DO-15_P2.54mm_Vertical_AnodeUp" H 6300 3900 50  0001 C CNN
F 3 "~" H 6300 3900 50  0001 C CNN
	1    6300 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3300 4600 3250
Wire Wire Line
	4600 3700 4700 3700
Wire Wire Line
	4600 3700 4600 3600
Wire Wire Line
	4050 4250 4050 4600
Connection ~ 4600 3700
Wire Wire Line
	4050 3700 4600 3700
Wire Wire Line
	4050 3950 4050 3700
$Comp
L Device:C C4
U 1 1 60C2DB03
P 4050 4100
F 0 "C4" H 4165 4146 50  0000 L CNN
F 1 "0.1m" H 4165 4055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 4088 3950 50  0001 C CNN
F 3 "~" H 4050 4100 50  0001 C CNN
	1    4050 4100
	1    0    0    -1  
$EndComp
Connection ~ 4050 3700
Wire Wire Line
	3750 3700 4050 3700
Wire Wire Line
	5100 4600 5750 4600
Wire Wire Line
	5100 4600 4700 4600
Connection ~ 5100 4600
Wire Wire Line
	5100 4400 5100 4600
$Comp
L Device:R R14
U 1 1 60C1F710
P 4600 3450
F 0 "R14" H 4670 3496 50  0000 L CNN
F 1 "0.22" H 4670 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4530 3450 50  0001 C CNN
F 3 "~" H 4600 3450 50  0001 C CNN
	1    4600 3450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R15
U 1 1 60C1F3F0
P 5700 3500
F 0 "R15" H 5770 3546 50  0000 L CNN
F 1 "180" H 5770 3455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5630 3500 50  0001 C CNN
F 3 "~" H 5700 3500 50  0001 C CNN
	1    5700 3500
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:MC34063AD U3
U 1 1 60C0A1E7
P 5100 3900
F 0 "U3" H 5100 4367 50  0000 C CNN
F 1 "MC34063AD" H 5100 4276 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5150 3450 50  0001 L CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MC34063A-D.PDF" H 5600 3800 50  0001 C CNN
	1    5100 3900
	1    0    0    -1  
$EndComp
Text GLabel 3750 3700 0    50   Input ~ 0
Vbat
$EndSCHEMATC
