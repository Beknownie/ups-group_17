EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Project-schematic-rework-rescue:USB_A-Connector J1
U 1 1 60B9632B
P 1200 3150
F 0 "J1" H 1257 3617 50  0000 C CNN
F 1 "USB_A" H 1257 3526 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Lumberg_2486_01_Horizontal" H 1350 3100 50  0001 C CNN
F 3 " ~" H 1350 3100 50  0001 C CNN
	1    1200 3150
	1    0    0    -1  
$EndComp
Text GLabel 1500 2950 2    50   Input ~ 0
Vcc
Text GLabel 1150 3600 3    50   Input ~ 0
Vgnd
NoConn ~ 1500 3150
NoConn ~ 1500 3250
Wire Wire Line
	1100 3550 1150 3550
Wire Wire Line
	1150 3600 1150 3550
Connection ~ 1150 3550
Wire Wire Line
	1150 3550 1200 3550
Text GLabel 1000 2250 3    50   Input ~ 0
Vbat
Text GLabel 1400 2250 3    50   Input ~ 0
Vgnd
$Comp
L Mechanical:MountingHole_Pad Bat+1
U 1 1 60C4AD21
P 1000 2150
F 0 "Bat+1" H 1100 2153 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 2108 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1000 2150 50  0001 C CNN
F 3 "~" H 1000 2150 50  0001 C CNN
	1    1000 2150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad Bat-1
U 1 1 60C4B5D3
P 1400 2150
F 0 "Bat-1" H 1500 2153 50  0000 L CNN
F 1 "MountingHole_Pad" H 1500 2108 50  0001 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1400 2150 50  0001 C CNN
F 3 "~" H 1400 2150 50  0001 C CNN
	1    1400 2150
	1    0    0    -1  
$EndComp
Text Notes 7350 7550 0    118  ~ 0
LED schematic 
Text GLabel 4800 5350 3    50   Input ~ 0
Vgnd
$Comp
L Device:R R9
U 1 1 60BDD18D
P 4800 5200
F 0 "R9" H 4870 5246 50  0000 L CNN
F 1 "1k" H 4870 5155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4730 5200 50  0001 C CNN
F 3 "~" H 4800 5200 50  0001 C CNN
	1    4800 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2650 4800 2750
Text GLabel 4800 2650 1    50   Input ~ 0
Vbat
$Comp
L Device:R R3
U 1 1 60BD8258
P 4400 3150
F 0 "R3" H 4470 3196 50  0000 L CNN
F 1 "9333" H 4470 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 3150 50  0001 C CNN
F 3 "~" H 4400 3150 50  0001 C CNN
	1    4400 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5150 4400 5300
Text GLabel 4400 5300 3    50   Input ~ 0
Vgnd
$Comp
L Device:R R7
U 1 1 60BCB81C
P 4400 5000
F 0 "R7" H 4470 5046 50  0000 L CNN
F 1 "3.25k" H 4470 4955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 5000 50  0001 C CNN
F 3 "~" H 4400 5000 50  0001 C CNN
	1    4400 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4350 5100 4350
Wire Wire Line
	4400 4850 5100 4850
Connection ~ 4400 4850
Wire Wire Line
	4400 4750 4400 4850
Wire Wire Line
	4400 4450 4400 4350
$Comp
L Device:R R6
U 1 1 60BC6B25
P 4400 4600
F 0 "R6" H 4330 4554 50  0000 R CNN
F 1 "250" H 4330 4645 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 4600 50  0001 C CNN
F 3 "~" H 4400 4600 50  0001 C CNN
	1    4400 4600
	-1   0    0    1   
$EndComp
Connection ~ 4400 4350
Wire Wire Line
	4400 4350 4400 4250
Wire Wire Line
	4400 3400 4400 3300
Wire Wire Line
	4400 3950 4400 3850
Wire Wire Line
	4400 3850 4400 3700
Connection ~ 4400 3850
Wire Wire Line
	5100 3850 4400 3850
$Comp
L Device:R R5
U 1 1 60BC06B4
P 4400 4100
F 0 "R5" H 4330 4054 50  0000 R CNN
F 1 "250" H 4330 4145 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 4100 50  0001 C CNN
F 3 "~" H 4400 4100 50  0001 C CNN
	1    4400 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 60BBF52E
P 4400 3550
F 0 "R4" H 4470 3596 50  0000 L CNN
F 1 "250" H 4470 3505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4330 3550 50  0001 C CNN
F 3 "~" H 4400 3550 50  0001 C CNN
	1    4400 3550
	1    0    0    -1  
$EndComp
Connection ~ 4400 3300
Wire Wire Line
	5100 3300 4400 3300
Wire Wire Line
	4400 3000 4400 2800
Wire Wire Line
	4800 2750 6800 2750
Connection ~ 4800 2750
Wire Wire Line
	4800 2900 4800 2750
$Comp
L Device:R R8
U 1 1 60BB05F5
P 4800 3050
F 0 "R8" H 4870 3096 50  0000 L CNN
F 1 "1.5k" H 4870 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4730 3050 50  0001 C CNN
F 3 "~" H 4800 3050 50  0001 C CNN
	1    4800 3050
	1    0    0    -1  
$EndComp
Connection ~ 4800 5050
Wire Wire Line
	4800 5050 5100 5050
Wire Wire Line
	4800 4550 4800 5050
Connection ~ 4800 4550
Wire Wire Line
	4800 4550 5100 4550
Wire Wire Line
	4800 4050 4800 4550
Connection ~ 4800 4050
Wire Wire Line
	4800 4050 5100 4050
Wire Wire Line
	4800 3200 4800 3500
Wire Wire Line
	4800 3500 4800 4050
Connection ~ 4800 3500
Wire Wire Line
	5100 3500 4800 3500
Wire Wire Line
	6800 4950 6650 4950
Wire Wire Line
	6800 4450 6800 4950
Connection ~ 6800 4450
Wire Wire Line
	6800 4450 6650 4450
Wire Wire Line
	6800 3950 6800 4450
Connection ~ 6800 3950
Wire Wire Line
	6800 3950 6650 3950
Wire Wire Line
	6800 2750 6800 3400
Wire Wire Line
	6800 3400 6800 3950
Connection ~ 6800 3400
Wire Wire Line
	6650 3400 6800 3400
Wire Wire Line
	6000 4950 6350 4950
Wire Wire Line
	6350 4450 6000 4450
Wire Wire Line
	6000 3950 6350 3950
Wire Wire Line
	6350 3400 6000 3400
$Comp
L Device:LED D5
U 1 1 60BA4E6E
P 6500 4950
F 0 "D5" H 6493 5075 50  0000 C CNN
F 1 "NSCW100" H 6493 5075 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 6500 4950 50  0001 C CNN
F 3 "~" H 6500 4950 50  0001 C CNN
	1    6500 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 60BA4AAD
P 6500 4450
F 0 "D4" H 6493 4575 50  0000 C CNN
F 1 "NSCW100" H 6493 4575 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 6500 4450 50  0001 C CNN
F 3 "~" H 6500 4450 50  0001 C CNN
	1    6500 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60BA474A
P 6500 3950
F 0 "D3" H 6493 4075 50  0000 C CNN
F 1 "NSCW100" H 6493 4075 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 6500 3950 50  0001 C CNN
F 3 "~" H 6500 3950 50  0001 C CNN
	1    6500 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60BA370F
P 6500 3400
F 0 "D2" H 6493 3525 50  0000 C CNN
F 1 "NSCW100" H 6493 3525 50  0001 C CNN
F 2 "LED_THT:LED_D3.0mm" H 6500 3400 50  0001 C CNN
F 3 "~" H 6500 3400 50  0001 C CNN
	1    6500 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 60BA32D3
P 5850 4950
F 0 "R13" V 5643 4950 50  0000 C CNN
F 1 "150" V 5734 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5780 4950 50  0001 C CNN
F 3 "~" H 5850 4950 50  0001 C CNN
	1    5850 4950
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 60BA2F9B
P 5850 4450
F 0 "R12" V 5643 4450 50  0000 C CNN
F 1 "150" V 5734 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5780 4450 50  0001 C CNN
F 3 "~" H 5850 4450 50  0001 C CNN
	1    5850 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 60BA2B6A
P 5850 3950
F 0 "R11" V 5643 3950 50  0000 C CNN
F 1 "150" V 5734 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5780 3950 50  0001 C CNN
F 3 "~" H 5850 3950 50  0001 C CNN
	1    5850 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 60BA1D07
P 5850 3400
F 0 "R10" V 5643 3400 50  0000 C CNN
F 1 "150" V 5734 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 5780 3400 50  0001 C CNN
F 3 "~" H 5850 3400 50  0001 C CNN
	1    5850 3400
	0    1    1    0   
$EndComp
Text GLabel 5450 3050 0    50   Input ~ 0
Vgnd
Text GLabel 6050 3050 2    50   Input ~ 0
Vcc
$Comp
L Amplifier_Operational:LM324A U2
U 5 1 60B92BDA
P 5750 3150
F 0 "U2" V 5425 3150 50  0000 C CNN
F 1 "LM324A" V 5516 3150 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5700 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5800 3350 50  0001 C CNN
	5    5750 3150
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:LM324A U2
U 4 1 60B915D5
P 5400 4950
F 0 "U2" H 5400 5317 50  0001 C CNN
F 1 "LM324A" H 5400 5226 50  0001 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5350 5050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5450 5150 50  0001 C CNN
	4    5400 4950
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324A U2
U 3 1 60B8F7E9
P 5400 4450
F 0 "U2" H 5400 4817 50  0001 C CNN
F 1 "LM324A" H 5400 4726 50  0001 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5350 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5450 4650 50  0001 C CNN
	3    5400 4450
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324A U2
U 2 1 60B8D137
P 5400 3950
F 0 "U2" H 5400 4317 50  0001 C CNN
F 1 "LM324A" H 5400 4226 50  0001 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5350 4050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5450 4150 50  0001 C CNN
	2    5400 3950
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324A U2
U 1 1 60B8B658
P 5400 3400
F 0 "U2" H 5400 3767 50  0001 C CNN
F 1 "LM324A" H 5400 3676 50  0001 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5350 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5450 3600 50  0001 C CNN
	1    5400 3400
	1    0    0    -1  
$EndComp
Text GLabel 4400 2800 1    50   Input ~ 0
Vout
$EndSCHEMATC
